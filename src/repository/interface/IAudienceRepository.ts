import Audience from '../../model/Audience'

export default interface IEventRepository {
  findById(audienceId: string): Promise<Audience | null>;
  findByUUID(audienceUUID: string): Promise<Audience | null>;
  update(audience: Audience): Promise<Audience | null>;
  updateByDBName(dbName: string, audience: Audience): Promise<Audience | null>;
}
