import mongoose from 'mongoose'
import IAudienceRepository from './interface/IAudienceRepository'
import Audience, { AudienceModel, AudienceModelWithCustomConnection } from '../model/Audience'

export default class AudienceRepository implements IAudienceRepository {

  async findByUUIDFromDBName(dbName: string, audienceUUID: string): Promise<Audience | null> {
    const mongooseConnection = await mongoose.connection.useDb(dbName)
    const audienceMongooseModel = await AudienceModelWithCustomConnection(mongooseConnection)
    const audienceMongoose = await audienceMongooseModel.findOne({ uuid: audienceUUID })
    const audience = new Audience()
    if (audienceMongoose == null) {
      return null
    }
    await audience.parseFromMongoose(audienceMongoose)
    return audience
  }

  async findByUUID(audienceUUID: string): Promise<Audience | null> {
    const audienceMongoose = await AudienceModel.findOne({ uuid: audienceUUID })
    if (audienceMongoose == null) {
      return null
    }
    const audience = new Audience()
    audience.parseFromMongoose(audienceMongoose)
    return audience
  }

  async findById(audienceId: string): Promise<Audience | null> {
    const audienceMongoose = await AudienceModel.findById(audienceId)
    if (audienceMongoose === null) {
      return null
    }
    const audience = new Audience()
    audience.parseFromMongoose(audienceMongoose)
    return audience
  }

  async update(audience: Audience): Promise<Audience | null> {
    const audienceMongoose = await AudienceModel.findByIdAndUpdate(audience.id, audience)
    const returnAudience = new Audience()
    if (audienceMongoose == null) {
        return null
    }
    returnAudience.parseFromMongoose(audienceMongoose)
    return returnAudience
  }

  async updateByDBName(dbName: string, audience: Audience): Promise<Audience | null> {
    const mongooseConnection = await mongoose.connection.useDb(dbName)
    const audienceMongooseModel = await AudienceModelWithCustomConnection(mongooseConnection)
    const audienceMongoose = await audienceMongooseModel.findByIdAndUpdate(audience.id, audience, { new: true })
    
    const returnAudience = new Audience()
    if (audienceMongoose == null) {
        return null
    }
    
    returnAudience.parseFromMongoose(audienceMongoose)
    return returnAudience
  }
}
