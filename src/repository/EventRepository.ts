import { Types } from 'mongoose'
import IEventRepository from './interface/IEventRepository'
import Event, { EventModel } from '../model/Event'

export default class EventRepository implements IEventRepository {
  async save(event: Event): Promise<Event> {
    const mongooseEvent = await EventModel.create(event)
    const returnEvent = new Event()
    returnEvent.parseFromMongoose(mongooseEvent)
    return returnEvent
  }

  async findById(eventId: string): Promise<Event | null> {
    const eventMongoose = await EventModel.findById(eventId)
    if (eventMongoose === null) {
      return null
    }
    const event = new Event()
    event.parseFromMongoose(eventMongoose)
    return event
  }

  async findByWaitingRoomId(waitingRoomId: string): Promise<Event[]> {
    const eventMongoose = await EventModel.find({ waitingRoomId: Types.ObjectId(waitingRoomId) })
    const event: Event[] = new Array<Event>()
    eventMongoose.forEach((eventMongooseModel) => {
      const eventTemp = new Event()
      eventTemp.parseFromMongoose(eventMongooseModel)
      event.push(eventTemp)
    })
    return event
  }

  async findByUUID(uuid: string): Promise<Event | null> {
    const eventMongoose = await EventModel.findOne({ uuid })
    if (eventMongoose === null) {
      return null
    }
    const event = new Event()
    event.parseFromMongoose(eventMongoose)
    return event
  }

  async find(findObj: any): Promise<Event[]> {
    const eventMongoose = await EventModel.find(findObj)
    const events = eventMongoose.map((event): Event => {
      const newEvent = new Event()
      newEvent.parseFromMongoose(event)
      return newEvent
    })
    return events
  }

  async all(): Promise<Event[]> {
    const eventMongoose = await EventModel.find()
    const events = eventMongoose.map((event): Event => {
      const newEvent = new Event()
      newEvent.parseFromMongoose(event)
      return newEvent
    })
    return events
  }
}
