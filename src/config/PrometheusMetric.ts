const { Gauge, Registry } = require('prom-client')
export const eventMetricRegister = new Registry()

export const currentQueueNumber = new Gauge({
    name: 'oqp_event_current_queue_number',
    help: 'current queue number of queue',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const maxSessionTime = new Gauge({
    name: 'oqp_event_max_session_time',
    help: 'max session time in millis',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const minSessionTime = new Gauge({
    name: 'oqp_event_min_session_time',
    help: 'min session time in millis',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const avgSessionTime = new Gauge({
    name: 'oqp_event_avg_session_time',
    help: 'average session time in millis',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const completedAudience = new Gauge({
    name: 'oqp_event_completed_audience',
    help: 'completed audience',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const sessionTimedoutAudience = new Gauge({
    name: 'oqp_event_session_timedout_audience',
    help: 'session-timedout audience',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const waitingAudience = new Gauge({
    name: 'oqp_event_waiting_audience',
    help: 'waiting audience',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const arrivalTimedoutAudience = new Gauge({
    name: 'oqp_event_arrival_timedout_audience',
    help: 'arrival-timedout audience',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})

export const servingAudience = new Gauge({
    name: 'oqp_event_serving_audience_audience',
    help: 'serving audience',
    labelNames: ['eventUUID'],
    registers: [eventMetricRegister]
})