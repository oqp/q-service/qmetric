import { Schema, SchemaTypes, Document } from 'mongoose'
import { mongoose } from '../config/mongoose'
import BaseModel from './BaseModel'

interface IAudience {
  [key: string]: any;
  id: string;
  uuid: string;
  queueNumber: number;
  telno: string;
  email: string;
  status: string;
  enteredQueueAt: Date;
  eligibledAt: Date;
  canceledAt: Date;
  servedAt: Date;
  completedAt: Date;
}

export interface IAudienceModel extends Document, IAudience {
  id: string;
  uuid: string;
  queueNumber: number;
  telno: string;
  email: string;
  status: string;
  enteredQueueAt: Date;
  eligibledAt: Date;
  canceledAt: Date;
  servedAt: Date;
  completedAt: Date;
}

export const AudienceSchema = new Schema({
  id: String,
  uuid: String,
  queueNumber: Number,
  telno: String,
  email: String,
  status: String,
  enteredQueueAt: Date,
  eligibledAt: Date,
  canceledAt: Date,
  servedAt: Date,
  completedAt: Date
})

export const AudienceModel = mongoose.model<IAudienceModel>('Audience', AudienceSchema)

export const AudienceModelWithCustomConnection =
  (mongooseConnection: mongoose.Connection): mongoose.Model<IAudienceModel> => {
    return mongooseConnection.model<IAudienceModel>('Audience', AudienceSchema)
}

export default class Audience extends BaseModel implements IAudience {
  [key: string]: any;
  id: string = '';
  uuid: string = '';
  queueNumber: number = 0;
  telno: string = '';
  email: string = '';
  status: string = '';
  enteredQueueAt: Date = new Date();
  eligibledAt: Date = new Date();
  canceledAt: Date = new Date();
  servedAt: Date = new Date();
  completedAt: Date = new Date();
}
