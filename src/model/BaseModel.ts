import { Document } from 'mongoose'

export default class BaseModel {
  id: string;

  parseFromMongoose(mongooseModel: Document): void {
    const modelKey = Object.keys(this).filter((value) => value !== 'id')
    this.id = mongooseModel._id
    modelKey.forEach((key: string) => {
      /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
      const updateKey: Record<string, any> = {}
      updateKey[key] = mongooseModel.toObject()[key]
      Object.assign(this, updateKey)
    })
  }
}
