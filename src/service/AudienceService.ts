import AudienceRepository from '../repository/AudienceRepository'
import Audience from '../model/Audience'

export default class EventService {
  audienceRepository: AudienceRepository;

  constructor() {
    this.audienceRepository = new AudienceRepository()
  }

  async findByUUID(eventUUID: string, audienceUUID: string): Promise<Audience | null> {
    return await this.audienceRepository.findByUUIDFromDBName(`event-${eventUUID}`, audienceUUID)
  }
}
