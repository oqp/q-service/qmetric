import EventRepository from '../repository/EventRepository'
import Event from '../model/Event'

export default class EventService {
  eventRepository: EventRepository;

  constructor() {
    this.eventRepository = new EventRepository()
  }

  async findById(eventId: string): Promise<Event | null> {
    const event = await this.eventRepository.findById(eventId)
    return event
  }

  async findByUUID(eventUUID: string): Promise<Event | null> {
    const event = await this.eventRepository.findByUUID(eventUUID)
    return event
  }

  async getAllEvent(): Promise<Event[]> {
    const events = await this.eventRepository.all()
    return events
  }

  async getAllActiveEvent(): Promise<Event[]> {
    const events = await this.eventRepository.find({
      isActive: true
    })
    return events
  }
}
