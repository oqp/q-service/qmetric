import mongoose from 'mongoose'
import { AudienceModelWithCustomConnection } from '../model/Audience'
import Event from '../model/Event'
import AudienceStatus from '../utils/AudienceStatus'
import EventService from './EventService'

export default class EventMetricService {

  eventService: EventService;

  constructor() {
    this.eventService = new EventService()
  }
  
  async getAverageSessionTime(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.aggregate([
      {
        $match: {
          $or: [{ status: "session-timedout"}, { status: "completed" }]
        }
      },
      {
          $addFields: {
              sessionTimeMillis: {
                  $cond: { if: { $eq: ["$status", "session-timedout"] }, then: { $subtract: ["$sessionTimedoutAt", "$servedAt"] }, else: { $subtract: ["$completedAt", "$servedAt"] } }
              }
          }
      },
      {
          $group: {
              _id: null,
              avgSessionTimeMillis: { $avg: "$sessionTimeMillis" }
          }
      }
    ])
    try {
      const avgResult = result[0]['avgSessionTimeMillis'] || 0
      return avgResult
    } catch {
      return 0
    }
  }

  async getMaxSessionTime(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.aggregate([
      {
        $match: {
          $or: [{ status: "session-timedout"}, { status: "completed" }]
        }
      },
      {
          $addFields: {
              sessionTimeMillis: {
                  $cond: { if: { $eq: ["$status", "session-timedout"] }, then: { $subtract: ["$sessionTimedoutAt", "$servedAt"] }, else: { $subtract: ["$completedAt", "$servedAt"] } }
              }
          }
      },
      {
          $group: {
              _id: null,
              maxSessionTimeMillis: { $max: "$sessionTimeMillis" }
          }
      }
    ])

    try {
      const maxResult = result[0]['maxSessionTimeMillis'] || 0
      return maxResult
    } catch {
      return 0
    }
  }

  async getMinSessionTime(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.aggregate([
      {
        $match: {
          $or: [{ status: "session-timedout"}, { status: "completed" }]
        }
      },
      {
          $addFields: {
              sessionTimeMillis: {
                  $cond: { if: { $eq: ["$status", "session-timedout"] }, then: { $subtract: ["$sessionTimedoutAt", "$servedAt"] }, else: { $subtract: ["$completedAt", "$servedAt"] } }
              }
          }
      },
      {
          $group: {
              _id: null,
              minSessionTimeMillis: { $min: "$sessionTimeMillis" }
          }
      }
    ])
    try {
      const minResult = result[0]['minSessionTimeMillis'] || 0
      return minResult
    } catch {
      return 0
    }
  }

  async getCompletedAudience(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.find({ status: AudienceStatus.COMPLETED }).countDocuments()
    return result || 0
  }

  async getSessionTimeoutAudience(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.find({ status: AudienceStatus.SESSION_TIMEDOUT }).countDocuments()
    return result || 0
  }

  async getWaitingAudience(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.find({ status: AudienceStatus.WAITING }).countDocuments()
    return result || 0
  }

  async getArrivalTimeoutAudience(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.find({ status: AudienceStatus.ARRIVAL_TIMEDOUT }).countDocuments()
    return result || 0
  }

  async getServingAudience(event: Event): Promise<Number> {
    const connection = await mongoose.connection.useDb(`event-${event.uuid}`)
    const audienceModel = await AudienceModelWithCustomConnection(connection)
    const result = await audienceModel.find({ status: AudienceStatus.SERVING }).countDocuments()
    return result || 0
  }

  async getCurrentQueueNumber(event: Event): Promise<Number> {
    const queryEvent = await this.eventService.findByUUID(event.uuid)
    if (queryEvent != null) {
      if (queryEvent.currentQueueNumber === null || queryEvent.currentQueueNumber === undefined) {
        return 0
      } else {
        return queryEvent.currentQueueNumber
      }
    } else {
      throw new Error('Event UUID Not found')
    }
  }
}
