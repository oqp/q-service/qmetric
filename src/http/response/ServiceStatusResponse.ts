export default class ServiceStatusResponse {
    codeClass: number
  
    messageClass: string
  
    isOKClass: boolean
  
    constructor() {
      this.codeClass = 0
      this.messageClass = ''
      this.isOKClass = false
    }
  
    set code(codeInput: number) {
      this.codeClass = codeInput
    }
  
    get code(): number {
      return this.codeClass
    }
  
    set message(messageInput: string) {
      this.messageClass = messageInput
    }
  
    get message(): string {
      return this.messageClass
    }
  
    set isOK(isOKInput: boolean) {
      this.isOKClass = isOKInput
    }
  
    get isOK(): boolean {
      return this.isOKClass
    }
  
    toJSON(): Record<string, any> {
      const json: Record<string, any> = {
        code: this.codeClass,
        message: this.messageClass,
        isOK: this.isOKClass,
      }
      return json
    }
  }
  