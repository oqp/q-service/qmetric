import express, { Router } from 'express'
import Event from './model/Event'
import EventMetricService from './service/EventMetricService'
import { mongoose } from './config/mongoose'
import { logger } from './config/logger'
import MetricsController from './controller/MetricsController'

const server = express();
const router = express.Router()
const PORT = process.env.PORT || 3000

server.use('/metrics', MetricsController)

server.get('/', (req, res) => {
  res.send(`Server is running! ${new Date()}`)
})

mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, { useNewUrlParser: true, connectTimeoutMS: 2000, useUnifiedTopology: true })
  .catch((message) => {
    logger.error({ message })
    process.exitCode = 1
    process.exit()
  })
mongoose.connection.once('open', () => {
  server.listen(PORT, () => {
    logger.info(`Connected to mongodb @ ${process.env.DB_HOST}`)
    logger.info(`Listening on ${PORT}`)
  })
})