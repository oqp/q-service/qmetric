import EventService from '../service/EventService'
import EventMetricService from '../service/EventMetricService'
import Event from '../model/Event'

import {
  currentQueueNumber,
  maxSessionTime,
  minSessionTime,
  avgSessionTime,
  completedAudience,
  sessionTimedoutAudience,
  waitingAudience,
  arrivalTimedoutAudience,
  servingAudience,
} from '../config/PrometheusMetric'

const updateEventMetric = async (event: Event, eventMetricService: EventMetricService) => {
  const eventUUID = event.uuid

  const currentQueueNumberResult = await eventMetricService.getCurrentQueueNumber(event)
  currentQueueNumber.set({ eventUUID }, currentQueueNumberResult)

  const maxSessionTimeResult = await eventMetricService.getMaxSessionTime(event)
  maxSessionTime.set({ eventUUID }, maxSessionTimeResult)

  const minSessionTimeResult = await eventMetricService.getMinSessionTime(event)
  minSessionTime.set({ eventUUID }, minSessionTimeResult)

  const avgSessionTimeResult = await eventMetricService.getAverageSessionTime(event)
  avgSessionTime.set({ eventUUID }, avgSessionTimeResult)

  const completedAudienceResult = await eventMetricService.getCompletedAudience(event)
  completedAudience.set({ eventUUID }, completedAudienceResult)

  const sessionTimedoutAudienceResult = await eventMetricService.getSessionTimeoutAudience(event)
  sessionTimedoutAudience.set({ eventUUID }, sessionTimedoutAudienceResult)

  const waitingAudienceResult = await eventMetricService.getWaitingAudience(event)
  waitingAudience.set({ eventUUID }, waitingAudienceResult)

  const arrivalTimedoutAudienceResult = await eventMetricService.getArrivalTimeoutAudience(event)
  arrivalTimedoutAudience.set({ eventUUID }, arrivalTimedoutAudienceResult)

  const servingAudienceResult = await eventMetricService.getServingAudience(event)
  servingAudience.set({ eventUUID }, servingAudienceResult)
}


export const updateAllEventMetrics = async () => {
  const eventService = new EventService()
  const eventMetricService = new EventMetricService()
  const eventArray = await eventService.getAllEvent()
  const promiseArray: Promise<any>[] = []
  eventArray.forEach(async event => {
    promiseArray.push(updateEventMetric(event, eventMetricService))
  })
  await Promise.all(promiseArray)
}