export default class AudienceStatus {
    static get COMPLETED(): string {
        return 'completed'
    }
    static get WAITING(): string {
        return 'waiting'
    }
    static get SERVING(): string {
        return 'serving'
    }
    static get ELIGIBLED(): string {
        return 'eligibled'
    }
    static get ARRIVAL_TIMEDOUT(): string {
        return 'arrival-timedout'
    }
    static get SESSION_TIMEDOUT(): string {
        return 'session-timedout'
    }
    static get CANCELED(): string {
        return 'canceled'
    }
}