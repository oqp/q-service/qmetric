require('dotenv').config()
import express from 'express'
import { logger } from '../config/logger'
import { updateAllEventMetrics } from '../prometheus/EventMetric'
import { eventMetricRegister } from '../config/PrometheusMetric'

const router = express.Router()

router.get('/', async (req, res) => {
    await updateAllEventMetrics()
    setTimeout(() => {
        res.set('Content-Type', eventMetricRegister.contentType);
        res.end(eventMetricRegister.metrics());
        setTimeout(() => {
            eventMetricRegister.resetMetrics()
        }, 100)
    }, 100)
})

export default router